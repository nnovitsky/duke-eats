This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify

This is a project by Victoria Lasarte, Tanvi Pabby, and Natalie Novitsky for our CS290 course at Duke University. The project was built to create a platform for Duke students to place and deliver orders at Duke on campus eateries in real time. 

Natalie was already working on JavaScript files for her another project, so it made sense for her to take the lead on the functionality of the app in JavaScript and HTML. This also included integrating the pages with Firebase. In addition to helping Natalie with the functionality when needed, Tanvi took the lead on most administrative tasks, like keeping the team on schedule with presentations, sprint reports, and weekly exercises. Tanvi also learned to use HTML and CSS to contribute to styling, and helped design many of the pages with the Bootstrap template. As Victoria had CSS and design experience, she took the lead on the design and styling. She spent the majority of her time working to understand the relationship between CSS and React Bootstrap. She worked to combine both these styling methods in order to modify the template to provide the user with both a simplistic but elegant experience. 

Also install node packages react-router-dom, react-bootstrap, firebase, node-sass, bootstrap, recompose, react-dom, and react-scripts if project is failing to run. ('npm install [package]')

All of the code from the SCSS was taken from https://startbootstrap.com/themes/grayscale/, however in all of the scss files we made a number of modifications in order to override or modify the template.

We followed this tutorial https://www.robinwieruch.de/complete-firebase-authentication-react-tutorial/
to implement user authentication in our app. We then used documentation from https://firebase.google.com/docs/web/setup to add more features. 